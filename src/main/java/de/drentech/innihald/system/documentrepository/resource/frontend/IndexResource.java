package de.drentech.innihald.system.documentrepository.resource.frontend;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@SessionScoped
@Named
public class IndexResource implements Serializable {
    private static final long serialVersionUID = 8693277383648025822L;

    public String doViewIndex() {
        System.out.println("View Index page");
        return "index";
    }
}
