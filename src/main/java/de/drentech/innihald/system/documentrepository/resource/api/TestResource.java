package de.drentech.innihald.system.documentrepository.resource.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Properties;
import javax.ws.rs.core.MediaType;

@Path("/test")
public class TestResource {

    @GET
    public String getTest() {
        return "test";
    }

    @GET()
    @Path("/properties")
    @Produces(MediaType.APPLICATION_JSON)
    public Properties getProperties() {
        return System.getProperties();
    }
}
